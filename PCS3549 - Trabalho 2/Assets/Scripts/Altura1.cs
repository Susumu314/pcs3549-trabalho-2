﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Altura1 : MonoBehaviour
{
    // Start is called before the first frame update
    private GameObject player;
    public Text hightText;
    void Start()
    {
        player  = GameObject.Find("Player1");
    }

    // Update is called once per frame
    void Update()
    {
        hightText.text = player.GetComponent<Player>().altura.ToString("0");
    }
}
