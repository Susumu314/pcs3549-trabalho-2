﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridScript : MonoBehaviour
{
    public float gridSize;
    Vector3 truePos;
    void LateUpdate()
    {
        foreach (Transform child in transform){
            GameObject target = child.gameObject;
            truePos.x = Mathf.Floor(target.transform.position.x/gridSize) * gridSize;
            truePos.y = Mathf.Floor(target.transform.position.y/gridSize) * gridSize;
            truePos.z = Mathf.Floor(target.transform.position.z/gridSize) * gridSize;

            target.transform.position = truePos;
        }
    }
}
