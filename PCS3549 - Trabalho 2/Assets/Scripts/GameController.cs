﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject Player1;
    public GameObject Player2;

    string scenename;
    private string state;
    private Vector3 P1pos;
    private Quaternion P1rot;
    private Vector3 P2pos;
    private Quaternion P2rot;




    void Start()
    {
        state = "corrida";
        scenename = SceneManager.GetActiveScene().name;
        Player1 = GameObject.Find("Player1");
        Player2 = GameObject.Find("Player2");
        //pega posicao inicial dos players
        P1pos = Player1.transform.position;
        P1rot = Player1.transform.rotation;
        P2pos = Player2.transform.position;
        P2rot = Player2.transform.rotation;

    }

    // Update is called once per frame
    void Update()
    {
        if (state == "corrida"){
            if (Player1.GetComponent<Player>().dead && Player2.GetComponent<Player2>().dead){
                state = "montagem";
                Player1.transform.position = P1pos;
                Player1.transform.rotation = P1rot;
                Player2.transform.position = P2pos;
                Player2.transform.rotation = P2rot;
            }
        }
        if (state == "montagem"){
            if(/*fim_da_montagem*/true){
                Player1.GetComponent<Player>().dead = false;
                Player2.GetComponent<Player2>().dead = false;
            }
        }
    }
}
