﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Velocidade1 : MonoBehaviour
{
    // Start is called before the first frame update
    private GameObject player;
    public Text speedText;
    void Start()
    {
        player  = GameObject.Find("Player1");
    }

    // Update is called once per frame
    void Update()
    {
        speedText.text = player.GetComponent<Player>().vQueda.ToString("0");
    }
}
