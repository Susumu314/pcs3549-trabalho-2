﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    // Start is called before the first frame update
    private GameObject player1;
    private GameObject player2;
    public Text scoreText;
    void Start()
    {
        player1  = GameObject.Find("Player1");
        player2  = GameObject.Find("Player2");
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = player1.GetComponent<Player>().score.ToString() + " - " + player2.GetComponent<Player2>().score.ToString();
    }
}
