﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Altura2 : MonoBehaviour
{
    // Start is called before the first frame update
    private GameObject player;
    public Text hightText;
    void Start()
    {
        player  = GameObject.Find("Player2");
    }

    // Update is called once per frame
    void Update()
    {
        hightText.text = player.GetComponent<Player2>().altura.ToString("0");
    }
}
