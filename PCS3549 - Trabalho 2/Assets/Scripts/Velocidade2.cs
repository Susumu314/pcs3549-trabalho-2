﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Velocidade2 : MonoBehaviour
{
    // Start is called before the first frame update
    private GameObject player;
    public Text speedText;
    void Start()
    {
        player  = GameObject.Find("Player2");
    }

    // Update is called once per frame
    void Update()
    {
        speedText.text = player.GetComponent<Player2>().vQueda.ToString("0");
    }
}
