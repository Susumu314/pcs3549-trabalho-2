﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player2 : MonoBehaviour
{
    public float aceleracao;
    public Rigidbody rb;
    public float gravidade_tubo;
    public bool dead = false;
    [HideInInspector]
    public int score = 0;
    private GameObject pool;
    [HideInInspector]
    public float altura;
    [HideInInspector]
    public float vQueda;
    void Start()
    {   
        rb = GetComponent<Rigidbody>();
        pool = GameObject.Find("Pool");
    }
    // Update is called once per frame
    void FixedUpdate()
    {   
        altura = transform.position.y - pool.transform.position.y;
        vQueda = -rb.velocity.y;
        if(!dead){
            if (rb.IsSleeping()){
                rb.WakeUp();
            }
            RaycastHit hit;
            RaycastHit Virado;
            if (Physics.Raycast(transform.position, -transform.right, out hit, 1.2f)){
                if (hit.collider.tag == "Gravidade0"){
                    rb.useGravity = false;
                    gravidade_tubo = 0.5f;
                }
                else {
                    rb.useGravity = true;
                    gravidade_tubo = 0.1f;
                }
                transform.rotation = Quaternion.FromToRotation(transform.right, hit.normal) * transform.rotation;
                rb.AddRelativeForce(-gravidade_tubo, 0, 0, ForceMode.VelocityChange);
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    rb.AddRelativeForce(new Vector3(50, 0, 0), ForceMode.Impulse);
                }

            }
            else if (Physics.Raycast(transform.position, transform.right, out Virado, 1.2f)){
                transform.rotation = Quaternion.FromToRotation(-transform.right, hit.normal) * transform.rotation;
            }
            else {
                rb.useGravity = true;
                gravidade_tubo = 0.1f;
            }
            rb.AddRelativeForce(0, aceleracao*Input.GetAxis("Acelerar2"), 0, ForceMode.VelocityChange);

            rb.AddRelativeForce(0, 0, aceleracao*0.5f*Input.GetAxis("Horizontal2"), ForceMode.VelocityChange);
            rb.AddRelativeTorque(aceleracao*0.5f*Input.GetAxis("Horizontal2"), 0, 0, ForceMode.VelocityChange);

            rb.AddRelativeForce(0, -aceleracao*Input.GetAxis("Desacelerar2"), 0, ForceMode.VelocityChange);
        }
        else
        {
            if(!rb.IsSleeping()){
                rb.velocity = new Vector3(0f,0f,0f); 
                rb.angularVelocity = new Vector3(0f,0f,0f);
                rb.Sleep();
            }
        }
    }
    void Dead(){
        Debug.Log("Voce morreu");
        dead  = true;
    }

    void Point(){
        Debug.Log("voce ganhou 1 ponto");
        dead = true;
    }

    void OnCollisionEnter(Collision collision){
        if(!dead){
            if (collision.collider.tag == "Trap"){
                Dead();
            }
            if (collision.collider.name == "Pool"){
                if (collision.relativeVelocity.y > 20){
                    Dead();
                }
                else{
                    Point();
                }
            }
        }
    }

}
